import time
import math
start_time = time.time()

def digit_factorial():
    working_numbers = []
    for i in range(10,math.factorial(8)+math.factorial(7)):
        list_num = []
        list_num[:0] = str(i)
        list_num.sort(reverse = True)
        sum_factorial = 0
        for num in list_num:
            sum_factorial += math.factorial(int(num))
        if sum_factorial == i:
            working_numbers.append(i)
    return sum(working_numbers)

print(digit_factorial())
print(f"--- {(time.time() - start_time):.10f} seconds ---" )